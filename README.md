# Procument Club
```
Pathway I, Practical II
ONT4202  [4206] 
Nelson Mandela University (2020)
Design Pattern : Composite
```
<br/>
Console Application to facilitate the procument of goods to the target business.  

The application maintains a list of **Cars**, to which the **Parts** in the **Parcels** being ordered belong.  
At the top most level in the hierachy is the **Order**, which cobtains the **List** of **Cars**.

At the bottom most level of the hierachy we have the **Parcel** that contains the **Parts** being ordered.

![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/58992956?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)

**Remote Repository :** [https://gitlab.com/Cosurmyne/procument-club.git](https://gitlab.com/Cosurmyne/procument-club.git)
