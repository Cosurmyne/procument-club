﻿namespace root
{
    class Program
    {
        static void Main(string[] args)
        {
            Product procument = new Order("Order List");
            Client focus = new Client(procument);

            byte choice = Utensil.Pavilion();
            while (choice != 0)
            {
                switch (choice)
                {
                    case 1: focus.GetList(); break;
                    case 2: focus.Procure(); break;
                    case 3: focus.Query(); break;
                }
                choice = Utensil.Pavilion();
            }
        }
    }
}
