namespace root
{
    public abstract class Product
    {

        public abstract string Description { get; }
        public abstract double Cost { get; }
        public abstract void DisplayInfo(bool root = false);

        public virtual Product this[int index]
        {
            get
            {
                throw new System.NotSupportedException("Sorry, not today.");
            }
        }

        public virtual void Add(Product item)
        {
            throw new System.NotSupportedException("Nothing to Add");
        }
        public virtual void Remove(Product item)
        {
            throw new System.NotSupportedException("Nothing to Remove");
        }
        public virtual void CalcuteCost()
        {
            throw new System.NotSupportedException("Nothing to Calculate.");
        }
        public virtual string[] Menu()
        {
            throw new System.NotSupportedException("Nothing to Calculate.");
        }
        public virtual int Count()
        {
            throw new System.NotSupportedException("Nothing to Count.");
        }
    }
}
