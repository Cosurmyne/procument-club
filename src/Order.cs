using System;
using System.Collections.Generic;

namespace root
{
    public class Order : Product
    {
        private string _description;
        private double _cost;
        private DateTime _year;

        private List<Product> _children = new List<Product>();

        public override string Description { get => _description; }
        public override double Cost { get => _cost; }
        public int Year { get => _year.Year; }

        public Order(string description, string year = "1973")
        {
            _description = description;
            _cost = 0;
            if (!DateTime.TryParse("06/04/" + year, out _year))
            {
                _year = DateTime.Today;
            }
        }

        public override void Add(Product item)
        {
            _children.Add(item);
        }

        public override void Remove(Product item)
        {
            _children.Remove(item);
        }

        public override Product this[int index]
        {
            get => _children[index];
        }

        public override void DisplayInfo(bool root = false)
        {
            string title = String.Format(" {0} ({1}) - {2:c}", Description.PadRight(21), Year, Cost);
            if (root)
            {
                title = String.Format(" {0} | Total Cost :: {1:c}", Description.PadRight(15), Cost);
                Console.WriteLine("{0}\n{1}", title, "".PadRight(title.Length, '='));
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("{0}\n{1}", title, "".PadRight(title.Length, '-'));
            }

            IEnumerator<Product> iterator = _children.GetEnumerator();
            while (iterator.MoveNext())
            {
                iterator.Current.DisplayInfo();
            }
        }

        public override void CalcuteCost()
        {
            double cost = 0;
            IEnumerator<Product> iterator = _children.GetEnumerator();
            while (iterator.MoveNext())
            {
                cost += iterator.Current.Cost;
            }
            _cost = cost;
        }

        public override int Count()
        {
            return _children.Count;
        }

        public override string[] Menu()
        {
            string[] list = new string[_children.Count];
            for (int i = 0; i < list.Length; i++) list[i] = _children[i].Description;
            return list;
        }
    }
}
