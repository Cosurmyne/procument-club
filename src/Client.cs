using System;
namespace root
{
    public class Client
    {
        Product list;

        public Client(Product list)
        {
            this.list = list;
        }

        public void Procure()
        {
            bool exit = false;
            do
            {
                switch (Utensil.Prompt("What are you Procuring?", new string[] { "New Car", "New Part" }, "Back"))
                {
                    case 0: exit = true; break;
                    case 1:
                        Console.Write("Name of car\t: ");
                        string name = Console.ReadLine();
                        Console.Write("Model Year\t: ");
                        string year = Console.ReadLine();
                        list.Add(new Order(name, year));
                        break;
                    case 2:
                        if (list.Count() == 0)
                        {
                            Console.WriteLine("You don't currently have a car to associate new Part with.");
                            Console.ReadKey();
                            break;
                        }
                        byte select = Utensil.Prompt("Select Car from List", list.Menu(), "Back");
                        Console.WriteLine();
                        Console.Write("{0}: ", "Part Description".PadRight(18));
                        string desc = Console.ReadLine();
                        bool valid = false;
                        double cost = Utensil.Digit(ref valid, String.Format("{0}: ", "Part Cost".PadRight(18)));
                        if (valid)
                        {
                            list[select - 1].Add(new Parcel(desc, cost));
                            list[select - 1].CalcuteCost();
                            list.CalcuteCost();
                        }
                        break;
                }
            } while (!exit);
        }

        public void GetList()
        {
            switch (list.Count())
            {
                case 0: Console.WriteLine("Nothing to See here!"); break;
                default:
                    Console.Clear();
                    list.DisplayInfo(true);
                    break;
            }
            Console.ReadKey();
        }

        public void Query()
        {
            if (list.Count() == 0)
            {
                Console.WriteLine("Nothing to See here!");
                Console.ReadKey();
                return;
            }
            byte select = Utensil.Prompt("Select Car from List", list.Menu(), "Back");
	    list[select -1].DisplayInfo();
	    Console.ReadKey();
        }
    }
}
