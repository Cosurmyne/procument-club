using System;

namespace root
{
    public static class Utensil
    {
        public enum T { ALL = 0, STAFF = 1, STUDENT = 2 };

        public static byte Pavilion()
        {
            string[] prompt = { "Print Report", "Make Order", "Targeted View" };
            return Utensil.Prompt("Procument Club", prompt, "Exit");
        }

        public static byte Prompt(string prompt, string[] options, string exit = null)
        {
            int val = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("{0}", prompt);
                Console.WriteLine("".PadRight(prompt.Length, '='));
                for (byte i = 1; i <= options.Length; i++) Console.WriteLine("- {0} :: {1}", i, options[i - 1]);
                Console.WriteLine();

                if (exit != null)
                {
                    Console.WriteLine("- {0} :: {1}", 0, exit);
                    Console.WriteLine();
                }
                Console.Write("selection >> ");
                if (!IsInt(Console.ReadLine(), ref val)) continue;
                if (val >= (exit != null ? 0 : 1) && val <= options.Length) break;
            } while (true);
            return (byte)val;
        }
	public static double Digit(ref bool valid, string prompt)
	{
		double digit = 0;
		Console.Write(prompt);
		try
		{
			digit = double.Parse(Console.ReadLine());
			valid = true;
		}
		catch (System.FormatException)
		{
			valid = false;
			ConsoleColor temp = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("Invalid Input!");
			Console.ForegroundColor = temp;
			Console.WriteLine("Let's try again");
			Console.ReadKey();
			return 0;
		}
		return digit;
	}

        public static bool IsInt(string input, ref int val)
        {
            try
            {
                val = int.Parse(input);
            }
            catch (System.FormatException)
            {
                return false;
            }
            return true;
        }
        public static bool IsFloat(string input, ref double val)
        {
            try
            {
                val = double.Parse(input);
            }
            catch (System.FormatException)
            {
                return false;
            }
            return true;
        }
    }
}
