using System;

namespace root
{
    public class Parcel : Product
    {
        private string _description;
        private double _cost;

        public Parcel(string description, double cost)
        {
            _description = description;
            _cost = cost;
        }

        public override string Description { get => _description; }

        public override double Cost { get => _cost; }

        public override void DisplayInfo(bool root = false)
        {
            Console.WriteLine(" - {0} : {1:c}", Description.PadRight(23), Cost);
        }

    }
}
